package protocol

import (
	"fmt"
	"sync"
)

// Tx 输出 chan
type Tx struct {
	Data chan interface{}
	Err chan error
	Stop chan int
}

var TXMap = make(map[string]*Tx)
var TXMapMutex sync.Mutex

// Set
func Set(requrst string) {
	TXMapMutex.Lock()
	defer TXMapMutex.Unlock()
	TXMap[requrst] = &Tx{
		Data: make(chan interface{}),
		Err: make(chan error),
		Stop: make(chan int),
	}
}

// Get
func Get(requrst string) (*Tx, error) {
	TXMapMutex.Lock()
	defer TXMapMutex.Unlock()
	tx, ok := TXMap[requrst]
	if ok {
		return tx, nil
	}
	return nil, fmt.Errorf("请求ID已经过期")
}

// Close
func Close(requrst string) {
	TXMapMutex.Lock()
	defer TXMapMutex.Unlock()
	tx := TXMap[requrst]
	tx, ok := TXMap[requrst]
	if !ok {
		return
	}
	close(tx.Data)
	close(tx.Err)
	close(tx.Stop)
	delete(TXMap, requrst)
}
