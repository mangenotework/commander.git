package handler

import (
	"fmt"
	"os"

	"gitee.com/mangenotework/commander/common/conf"
	"gitee.com/mangenotework/commander/common/protocol"
	"gitee.com/mangenotework/commander/master/dao"

	"github.com/gin-gonic/gin"
)

// CacheSize
func CacheSize(c *gin.Context) {
	f, err := os.Open(conf.MasterConf.DBPath.Data)
	if err != nil {
		APIOutPut(c, 1, 0, err.Error(), "")
		return
	}
	fi, err := f.Stat()
	if err != nil {
		APIOutPut(c, 1, 0, err.Error(), "")
		return
	}
	APIOutPut(c, 1, 0, fi.Size(), "")
	return
}

// CacheList
func CacheList(c *gin.Context) {
	list := dao.TableAll
	slaveList := protocol.AllUdpClient.GetAllKey()
	for _, v := range slaveList{
		tableName := fmt.Sprintf(dao.PerformanceBucketName, v)
		if dao.HasTable(tableName) {
			list = append(list, tableName)
		}
	}
	APIOutPut(c, 0, 1, list, "")
	return
}

// CacheDelete
func CacheDelete(c *gin.Context) {
	name := c.Query("name")
	err := dao.ClearTable(name)
	if err != nil {
		APIOutPut(c, 0, 1, "", "刪除失敗:"+err.Error())
		return
	}
	err = dao.CreateTable(name)
	if err != nil {
		APIOutPut(c, 0, 1, "", "刪除失敗:"+err.Error())
		return
	}
	APIOutPut(c, 0, 1, "", "刪除成功!")
	return
}

// OperateList
func OperateList(c *gin.Context) {
	data, _ := new(dao.DaoOperate).GetALL()
	APIOutPut(c, 0, 1, data, "")
	return
}

// OperateDelete
func OperateDelete (c *gin.Context) {
	date := c.Query("date")
	err := new(dao.DaoOperate).Del(date)
	if err != nil {
		APIOutPut(c, 1, 0, "", err.Error())
		return
	}
	APIOutPut(c, 0, 0, "", "ok")
	return
}
