package main

import (
	"net/http"
	_ "net/http/pprof"

	"gitee.com/mangenotework/commander/common/check"
	"gitee.com/mangenotework/commander/common/conf"
	"gitee.com/mangenotework/commander/common/logger"
	"gitee.com/mangenotework/commander/master/dao"
	"gitee.com/mangenotework/commander/master/http_server"
	"gitee.com/mangenotework/commander/master/udp_server"
)

func main(){
	conf.MasterInitConf()
	check.MasterInitPath()
	dao.DBInit()
	logger.Info("启动服务......")
	udp_server.RunUDPServer()
	http_server.RunHttpServer()
	go func() {
		http.ListenAndServe("127.0.0.1:6060", nil)
	}()
	select {}
}