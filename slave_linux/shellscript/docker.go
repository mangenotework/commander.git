package shellscript

func InstallDocker() string{
	return `#! /bin/bash
containerName=$1
imageName=$2
if [[ $containerName && $imageName ]]
then
  containerId=$(docker ps -aqf "name=${containerName}")
  imageId=$(docker images -q ${imageName})
  echo containerId=$containerId,imageId=$imageId
  if [[ $containerId && $imageId ]]
  then
    if [ ${#containerId} -lt 15 ]
    then
      docker stop $containerId
      docker rm $containerId
      docker rmi $imageId
    else
      echo rm too many contaninerId is dangerous
    fi
  else
    echo nothing need to rm
  fi
  cd /usr/local/src/springboot
  docker build -t ${imageName}:1.0 -f Dockerfile .
  newImageId=$(docker images -q ${imageName})
  docker run -itd -p 8888:8888 --name $containerName $newImageId
  echo work done
else
  echo containerName and imageName must not be null
fi
`
}